package pl.edu.pwsztar;

public class ShoppingListProduct {
    private final String nameProduct;
    private final int value;
    private int quantity;

    public ShoppingListProduct(String nameProduct, int value, int quantity) {
        this.nameProduct = nameProduct;
        this.value = value;
        this.quantity = quantity;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public int getValue() {
        return value;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
