package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {

    private final List<ShoppingListProduct> productList;

    public ShoppingCart() {
        this.productList = new ArrayList<>();
    }

    public boolean addProducts(String nameProduct, int value, int quantity) {
        if(getAllProductsQuantity() + quantity >= ShoppingCart.PRODUCTS_LIMIT || value <= 0 || quantity <= 0){
            return false;
        }
        nameProduct = nameProduct.toLowerCase();

        if(isProductAlreadyAdded(nameProduct)){
            for(ShoppingListProduct product: productList){
                if(product.getNameProduct().equals(nameProduct) && product.getValue() != value){
                    product.setQuantity(product.getQuantity() + quantity);
                    return true;
                }
            }
        }else{
            productList.add(new ShoppingListProduct(nameProduct, value, quantity));
            return true;
        }
        return false;
    }

    public boolean deleteProducts(String nameProduct, int quantity) {
        if(quantity <=0 ){
            return false;
        }
        nameProduct = nameProduct.toLowerCase();
        for(ShoppingListProduct product: productList){
            if(product.getNameProduct().equals(nameProduct)){
                if (quantity >= product.getValue()) {
                    if(product.getQuantity() == quantity){
                        productList.remove(product);
                        return true;
                    }
                } else {
                    product.setQuantity(product.getQuantity() - quantity);
                    return true;
                }

            }
        }
        return false;
    }

    public int getQuantityOfProduct(String nameProduct) {
        return productList.stream().filter(product -> product.getNameProduct().equals(nameProduct.toLowerCase())).mapToInt(ShoppingListProduct::getQuantity).sum();
    }

    public int getSumProductsPrices() {
        return productList.stream().mapToInt(product -> product.getValue() * product.getQuantity()).sum();
    }

    public int getProductPrice(String productName) {
        productName = productName.toLowerCase();

        for(ShoppingListProduct product: productList){
            if(product.getNameProduct().equals(productName)){
                return product.getValue();
            }
        }
        return 0;
    }

    public List<String> getProductsNames() {
        return productList.stream().map(product -> product.getNameProduct().substring(0,1).toUpperCase() + product.getNameProduct().substring(1)).collect(Collectors.toList());
    }

    private boolean isProductAlreadyAdded(String name){
        return productList.stream().anyMatch(product -> product.getNameProduct().equals(name));
    }

    private  int getAllProductsQuantity(){
        return productList.stream().mapToInt(product -> product.getValue() * product.getQuantity()).sum();
    }
}
