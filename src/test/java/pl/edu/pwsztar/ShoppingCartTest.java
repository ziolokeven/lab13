package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.junit.jupiter.api.Assertions.*;

public class ShoppingCartTest {

    @DisplayName("Should adding product to Shopping List")
    @ParameterizedTest
    @CsvSource({
            "Snaks, 5, 3",
            "Watermelon, 4, 2",
            "Candies, 3, 4",
            "Icecream, 2, 1",
            "Lolipop, 1, 5"
    })
    void shouldAddProductToShoppingCart(String nameProduct, int value, int quantity) {

        final ShoppingCart shoppingCart = new ShoppingCart();
        final boolean result = shoppingCart.addProducts(nameProduct, value, quantity);
        assertTrue(result);
    }

    @DisplayName("should not  adding product to Shopping List")
    @ParameterizedTest
    @CsvSource({
            "Snaks, -5, 3",
            "Watermelon, -4, 2",
            "Candies, 0, 4",
            "Icecream, 2,-1",
            "Lolipop, 1, 0"
    })
    void shouldNotAddProductToShoppingCart(String nameProduct, int value, int quantity) {
        final ShoppingCart shoppingCart = new ShoppingCart();

        final boolean result = shoppingCart.addProducts(nameProduct, value,quantity);

        assertFalse(result);
    }

    @DisplayName("Should delete quantity/product from Shopping List")
    @ParameterizedTest
    @CsvSource({
            "Snaks, 2",
            "Watermelon, 1",
            "Candies, 2",
            "Icecream, 1",
            "Lolipop, 3"
    })
    void shouldDeleteProductFromShoppingCart(String nameProduct, int quantity) {
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Snaks", 5, 3);
        shoppingCart.addProducts("Watermelon", 4, 2);
        shoppingCart.addProducts("Candies", 3, 4);
        shoppingCart.addProducts("Icecream", 2, 1);
        shoppingCart.addProducts("Lolipop", 6, 5);

        final boolean result = shoppingCart.deleteProducts(nameProduct, quantity);

        assertTrue(result);
    }

    @DisplayName("Should not delete quantity/product from Shopping List")
    @ParameterizedTest
    @CsvSource({
            "Snaks, 5",
            "Watermelon, 5",
            "Candies, 5",
            "Icecream, 2",
            "Lolipop, 6"
    })
    void shouldNotDeleteProductFromShoppingCart(String nameProduct, int quantity) {
        final ShoppingCart shoppingCart = new ShoppingCart();

        shoppingCart.addProducts("Snaks", 5, 3);
        shoppingCart.addProducts("Watermelon", 4, 2);
        shoppingCart.addProducts("Candies", 3, 4);
        shoppingCart.addProducts("Icecream", 2, 1);
        shoppingCart.addProducts("Lolipop", 1, 5);

        final boolean result = shoppingCart.deleteProducts(nameProduct, quantity);

        assertFalse(result);
    }

    @DisplayName("Should show quantity product from Shopping List")
    @ParameterizedTest
    @CsvSource({
            "Snaks, 3",
            "Watermelon, 2",
            "Candies, 4",
            "Icecream, 1",
            "Lolipop, 5"
    })
    void shouldReturnQuantityOfProductFromShoppingCart(String nameProduct, int quantity) {
        final ShoppingCart shoppingCart = new ShoppingCart();

        shoppingCart.addProducts("Snaks", 5, 3);
        shoppingCart.addProducts("Watermelon", 4, 2);
        shoppingCart.addProducts("Candies", 3, 4);
        shoppingCart.addProducts("Icecream", 2, 1);
        shoppingCart.addProducts("Lolipop", 1, 5);

        final int result = shoppingCart.getQuantityOfProduct(nameProduct);

        assertEquals(result, quantity);
    }

    @DisplayName("Should not show quantity product from Shopping List")
    @Test
    void shouldReturnSumOfProductPriceFromShoppingCart() {

        final ShoppingCart shoppingCart = new ShoppingCart();

        shoppingCart.addProducts("Snaks", 5, 3);
        shoppingCart.addProducts("Watermelon", 4, 2);
        shoppingCart.addProducts("Candies", 3, 4);
        shoppingCart.addProducts("Icecream", 2, 1);
        shoppingCart.addProducts("Lolipop", 1, 5);

        final int result = shoppingCart.getSumProductsPrices();

        assertEquals(42, result);
    }

    @DisplayName("Should show value of product from Shopping List")
    @ParameterizedTest
    @CsvSource({
            "Snaks, 5",
            "Watermelon, 4",
            "Candies, 3",
            "Icecream, 2",
            "Lolipop, 1"
    })
    void shouldReturnProductPriceFromShoppingCart(String nameProduct, int value) {

        final ShoppingCart shoppingCart = new ShoppingCart();

        shoppingCart.addProducts("Snaks", 5, 3);
        shoppingCart.addProducts("Watermelon", 4, 2);
        shoppingCart.addProducts("Candies", 3, 4);
        shoppingCart.addProducts("Icecream", 2, 1);
        shoppingCart.addProducts("Lolipop", 1, 5);

        final int result = shoppingCart.getProductPrice(nameProduct);

        assertEquals(result, value);
    }

    @DisplayName("Should show productname from Shopping List")
    @Test
    void shouldReturnNamesOfProductFromShoppingCart() {
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Snaks", 5, 3);
        shoppingCart.addProducts("Watermelon", 4, 2);
        shoppingCart.addProducts("Candies", 3, 4);
        shoppingCart.addProducts("Icecream", 2, 1);
        shoppingCart.addProducts("Lolipop", 1, 5);

        List<String> products = new ArrayList<>();
        products.add("Snaks");
        products.add("Watermelon");
        products.add("Candies");
        products.add("Icecream");
        products.add("Lolipop");

        final List<String> result = shoppingCart.getProductsNames();

        assertEquals(products, result);
    }
}

